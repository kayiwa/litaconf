Programming involves writing a series of instructions for the computer to follow. The simplest sequence is one containing a single command. We will try out some of these now. You are encouraged to follow along with me and thank you for stopping me if something doesn't make sense. Let's start the interactive Python prompt (sometimes referred to as "interpreter") by running python on your terminal.

```
:::bash
$ python
Python 2.7.8 (default, Sep  9 2014, 22:08:43)
[GCC 4.9.1] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> print 'Hello, world!'
Hello, world!
>>>
```

Notice is that you don't need to type the space between the >>> and the 'print' - Python puts that there for you. Secondly Python cares about details (sometimes referred to as case-sensitive) like whether or not you use upper or lower case. If you typed Print instead of print you would get an error because Python considers the two words to be different.

Let's deliberately try to recreate this error.

```
:::bash
>>> Print "Hello, World!"
  File "<stdin>", line 1
    Print "Hello, World!"
                    ^
SyntaxError: invalid syntax
>>>
```

The `print` command is the way to get Python to display its results to you. In this case it is printing the sequence of characters H,e,l,l,o, ,W,o,r,l,d,!. Such a sequence of characters is known in programming circles as a string of characters or a plain string.

You signify a string by surrounding it in quotes. In Python you can use either single quotes (as above) or double quotes: "a string". This allows you to include one type of quote within a string which is surrounded by the other type - useful for apostrophes:

It's not just alphabet characters that can be printed:

```
:::bash
>>> print 20 + 22
42
```

Here we have printed the result of an arithmetic operation - we added twenty and twenty two. The Python interpreter recognized the numbers as such and the plus sign and did the sum for us. It then printed the result.

We can combine multiple expressions like this:

```
:::bash
>>> print ((6 * 5) + (7 - 5)) / (7 + 1)
```

As a general rule it's safest to include the brackets to make sure you get what you want when dealing with long series of sums like this.

Notice the way I used parentheses to group the numbers together. What happens if you type the same sequence without the parentheses? This is because Python will evaluate the multiplication and division before the addition and subtraction. All programming languages have rules to determine the sequence of evaluation of operations and this is known as operator precedence. You will need to look at the reference documentation for each language to see how it works. Python generally follows the rules of arithmetic, but occasionally it won't be as the example below shows.

```
:::bash
>>> print 7/2
3
```

results in a whole number (integer) result (i.e. 2). This is because Python sees that the numbers are whole numbers and assumes you want to keep them that way. If you want decimal fractions as a result simply write one number as a decimal:

Repeat the example above with 7.0 in place of 7

Python sees the 7.0 and realizes that we are happy dealing with fractions (referred to as real numbers or floating point), so it responds with a fractional result.

You've seen that we can print strings and numbers. Now we combine the two in one print statement, separating them with a comma. We can extend this feature by combining it with a useful Python trick for outputting data called a format string:

```
:::bash
>>> print "The sum of %d and %d is: %d" % (7,18,7+18)
```

In this command the format string contains '%' markers within it. The letter 'd' after the % tells Python that a 'decimal number' should be placed there. The values to fill in the markers are obtained from the values inside the bracketed expression following the % sign on its own.

There are other letters that can be placed after the % markers. Some of these include:

```
%s - for string
%0.2f - for a real number with a maximum of 2 decimal places
%04d - pad the number out to 4 digits with 0's
```

Finally type the following.

```
:::bash
>>> import sys
>>> import re
```

It appears to do nothing at all or at least there's no useful echo back. This is what actually happens when you run this.

When you start Python there are a bunch of commands available to you called built-ins, because they are built in to the Python core. However Python can extend the list of commands available by incorporating extension modules. This is the equivalent of getting the core lego set but extending it to do even cooler things.

In fact what this command does is makes available a whole bunch of new 'tools' in the shape of Python commands which are defined in a file called 'sys.py'. This is how Python is extended to do all sorts of clever things that are not built in to the basic system. You can even create your own modules and/or import and use them, for this workshop we will be importing the pymarc module which deals with MARC records.

An example of using the imported modules is

```
:::bash
>>> sys.exit()
```

This will close your interpreter.

Notice that exit had 2 brackets after it. That's because exit is a function defined in sys and when we call a Python function we need to supply the parentheses even if there's nothing inside them.

The usual way you can exit the interpreter is by hitting your platform's <end of input> key combination. On Windows this is <ctrl-z><enter>. On Linux or Mac OSX it is <ctrl-d>.

### Exercises

Try a few more calculations. Use some other arithmetic operators:

```
subtract (-)
multiply (*)
divide (/)
```

Experiment and you will soon get the idea.

```
:::bash
>>> print 'The total is: ', 25+45
```

-	Exit the interpreter
-	Go into the exercises directory and open to edit the helloworld.py file and make it edit the file to do the same thing we did in the interpreter.
-	Alter the script to print anything of your choosing.
-	Create a python script to print hello, world! four times.
-	Run the script from the command line

Collections Types: Lists
========================

Python has a rich family tree of collections. We will look at Lists, Tuples and Dictionaries which are the most commonly used.

Lists
-----

A list is a variable length sequence of Python objects. This is the most flexible kind of sequence; it can contain any kind of Python data object. It can grow or shrink as needed. We often use lists to collect a set of data elements like cards in a blackjack hand: we get two cards to start, and then more and more cards as we ask for a hit.

We are all familiar with lists in everyday life. A list is just a sequence of items. We can add items to a list or remove items from the list. Usually, where the list is written paper we can't insert items in the middle of a list only at the end. However if the list is in electronic format - in a word processor say - then we can insert items anywhere in the list.

We can also search a list to check whether something is already in the list or not. But you have to find the item you need by stepping through the list from front to back checking each item to see if it's the item you want. Lists are a fundamental collection type found in many modern programming languages.

Let’s look at this definition in detail.

-	Since a list is a sequence, all of the common operations and built-in functions of sequences apply. This includes +, * and [].
-	Since a list is mutable, it can be changed. Items can be added to the list or removed from the list. Unlike strings, these operations do not create a new list, but change the state of a list.
-	Since lists are an extension to the basic sequence type, lists have additional method functions.

List Operations
---------------

Python provides many operations on collections. Nearly all of them apply to Lists and a subset apply to other collection types, including strings which are just a special type of list - a list of characters. To create and access a list in Python we use square brackets. You can create an empty list by using a pair of square brackets with nothing inside, or create a list with contents by separating the values with commas inside the brackets:

Start a new python interpreter session and follow along with these examples. If something doesn’t work, or confuses you, ask.

```
:::bash
>>> lines = ['student_count','ip_address','session','connections']
>>> lines
['student_count', 'ip_address', 'session', 'connections']
>>> integers = [0, 1, 2, 3, 4, 5, 6, 7]
>>> 2*["pass","don't","pass"]
['pass', "don't", 'pass', 'pass', "don't", 'pass']
>>> len(lines)
4
>>> anotherList = []
>>> print another
[]

```

We can access the individual elements using an index number, where the first element is 0, inside square brackets. For example to access the third element, which will be index number 2 since we start from zero, we do this:

```
:::bash
>>> print lines[2]
session
```

We can also change the values of the elements of a list in a similar fashion:

```
:::bash
>>> lines[2] = limits
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'limits' is not defined
lines[2] = "limits"
>>> lines
['student_count', 'ip_address', 'limits', 'connections']
```

Notice that the third element (index 2) changed from "session" to "limits". You can use negative index numbers to access members from the end of the list. This is most commonly done using -1 to get the last item:

```
:::bash
>>> print lines[-1]
connections
```

We can add new elements to the end of a list using the append() operator:

```
:::bash
>>> integers.append(8)
>>> integers
[0, 1, 2, 3, 4, 5, 6, 7, 8]
```

We can even hold one list inside another as seen below:

```
:::bash
>>> anotherList.append(integers)
>>> anotherList
[[0, 1, 2, 3, 4, 5, 6, 7, 8]]
```

List indexing
-------------

.. image:: litaconf/src/master/images/listindices.png :alt: Image showing indices aligned with spaces before each item in list

-	`simplelist[i]` looks up the index in the above scheme and gets the next item
-	`simplelist[-i]` looks up the index in the second line and gets the next item

```
:::bash
>>> counts = [0,1,2,3,4]
>>> counts[0]
0
>>> counts[1]
1
>>> counts[2]
2
>>> counts[-1]
4
>>> counts[-2]
3
>>> counts[-4]
1
>>> counts[-5]
0
>>> counts[-6]
Traceback (most recent call last):
 File "<stdin>", line 1, in <module>
IndexError: list index out of range
>>>
```

List slicing
------------

-	`simpleList[a:m]` looks up the index `a`, until it reaches the index `m`
-	you can leave off the index for start/end
	-	`simpleList[:m]` etrieves all items from start (index 0) until we reach `z`, this is conveniently, the first m items.
	-	`simplelist[a:]` starts at index `a` and retrieves all items until we reach the end, this "skips" the first `a` items

```
>>> counts = [1, 2, 3, 4, 5]
>>> counts
[1, 2, 3, 4, 5]
>>> counts[1:]
[2, 3, 4, 5]
>>> counts[:-1]
[1, 2, 3, 4]
>>> counts[1:-1]
[2, 3, 4]
>>> counts[99:]
[]
>>> counts[:-99]
[]
>>> counts[3:8]
[4, 5]
```

Tuple
-----

Not every language provides a tuple construct but in those that do it's extremely useful. A tuple is really just an arbitrary collection of values which can be treated as a unit. In many ways a tuple is like a list, but with the significant difference that tuples are immutable which is to say that you can't change them nor append to them once created. In Python, tuples are simply represented by parentheses containing a comma separated list of values, like so:

.. code:: hash

```
>>> immutableList = (41,43,45)
>>> immutableList[1] # use indexing like a list
43
>>> immutableList[43] = 42
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
```

The main things to remember are that while parentheses are used to define the tuple, square brackets are used to index it and you can't change a tuple once its created. Otherwise most of the list operations also apply to tuples.

Finally, although you cannot change a tuple you can effectively add members using the addition operator because this actually creates a new tuple. Like this:

```
>>> anewTuple1 = (41,42,43)
>>> anewTuple2 = anewTuple1 + (44,)
>>> anewTuple2
(41, 42, 43, 44)
```

If we didn't use the trailing comma after the 44 then Python would have interpreted it as the integer 44 inside parentheses, not as a true tuple. But since you can't add integers to tuples it results in an error, so we add the comma to tell Python to treat the parentheses as a tuple. Any time you need to persuade Python that a single entry tuple really is a tuple add a trailing comma as we did here.

### Exercise

-	edit the file `exercises/basicexercise.py` and then run it.
-	edit the file `exercises/basicsliceexercise.py` and run it.

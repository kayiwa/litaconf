Functions
=========

This is a section that one can completely ignore and successfully write programs. One of the core principles of any programming language is, "Don't Repeat Yourself". If you have an action that should occur many times, you can define that action once and then call that code whenever you need to carry out that action. Modules and functions are useful as programs get larger and one needs to keep track of potential numerous moving parts.

Functions
---------

Functions are a set of actions that we group together, and give a name to. You have already used a number of functions from the core Python language, such as string.title() and list.sort(). We can define our own functions, which allows us to "teach" Python new behavior.

General Syntax
--------------

```
:::python
# Let's define a function.
def function_name(argument_1, argument_2):
   # Do whatever we want this function to do,
   #  using argument_1 and argument_2
```

Defining a function
-------------------

-	Give the keyword def, which tells Python that you are about to define a function.
-	Give your function a name. A variable name tells you what kind of value the variable contains; a function name should tell you what the function does.
-	Give names for each value the function needs in order to do its work.
	-	These are basically variable names, but they are only used in the function.
	-	They can be different names than what you use in the rest of your program.
	-	These are called the function's arguments.
-	Make sure the function definition line ends with a colon.
-	Inside the function, write whatever code you need to make the function do its work. -

```
:::python
# Use function_name to call the function.
    function_name(value_1, value_2)
```

Using your function
-------------------

-	To call your function, write its name followed by parentheses.
-	Inside the parentheses, give the values you want the function to work with.
	-	These can be variables such as current_name and current_age, or they can be actual values such as 'money' and 5.

Please launch your terminal:

```
:::python
print("You are doing good work, Francis!")
print("Thank you very much for your efforts on teaching python.")

print("\nYou are doing good work, Eric!")
print("Thank you very much for your efforts on teaching python.")

print("\nYou are doing good work, Jane!")
print("Thank you very much for your efforts on teaching python.")
```

Functions take repeated code, put it in one place, and then you call that code when you want to use it. Here's what the same program looks like with a function.

```
:::python
def thank_you(name):
# This function prints a two-line personalized thank you message.
    print("\nYou are doing good work, %s!" % name)
    print("Thank you very much for your efforts on this project.")

thank_you('Francis')
thank_you('Eric')
thank_you('Jane')
```

In our original code, each pair of print statements was run three times, and the only difference was the name of the person being thanked. When you see repetition like this, you can usually make your program more efficient by defining a function.

The keyword `def` tells Python that we are about to define a function. We give our function a name, thank_you() in this case. A variable's name should tell us what kind of information it holds; a function's name should tell us what the variable does. We then put parentheses. Inside these parentheses we create variable names for any variable the function will need to be given in order to do its job. In this case the function will need a name to include in the thank you message. The variable name will hold the value that is passed into the function thank_you().

To use a function we give the function's name, and then put any values the function needs in order to do its work. In this case we call the function three times, each time passing it a different name.

### Exercises

-

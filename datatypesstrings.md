Primitive Data Types : Strings
==============================

Primitive data types are so called because they are the most basic types of data we can manipulate. More complex data types are really combinations of the primitive types. These are the building blocks upon which all the other types are built, the very foundation of computing. They include letters, numbers and something very familiar to many who work in the library space, boolean types.

We've already seen these. They are literally any string or sequence of characters that can be printed on your screen. (In fact there can even be non-printable control characters too).

In Python, strings can be represented in with single double and triple quotes (triple quotes are used for building documentation for Python functions). Python accepts both double quotes and single quotes to delineate strings. In this pre-conference workshop, we will try use the convention that double quotes are used for strings of multiple characters and single quotes for strings consisting of a single character.:

```
>>> "hello, world!"
'hello, world!'
>>> "x\nx"
'x\nx'
>>> "\"z\""
'"z"'
>>> ""
''
""" Here is a very long string that spans several lines and Python will
    preserve the lines as we type them. This is useful in explaining what your code does"""
```

Characters in a string can be escaped (or quoted) with the backslash character, which changes the meaning of some characters. For example, the character n, in a string refers to the letter n while the character sequence \n refers to the newline character. A backslash also changes the meaning of the letter t, converting it into a tab character. You can also quote single and double quotes with backslashes. When other characters are escaped, it is assumed the backslash is a character of the string and it is escaped (with a backslash) in the result:

```
>>> "\z"
'\\z'
```

String Operations
-----------------

There are a number of operations that can be performed on strings. Some of these are built in to Python but many others are provided by modules that you must import (as we did with `import sys` earlier). These are some of the simple ones:

```
>>> print 'Forever and ' + 'Young' # string concatenation
Forever and again
>>> print 'Repeat ' * 3   # string repetition
Repeat Repeat Repeat
>>> print 'Again ' + ('and again ' * 3)  # combine '+' and '*'
Again and again and again and again
```

There are lots of other things we can do with strings but this serves as a foundational introduction.

### Exercise

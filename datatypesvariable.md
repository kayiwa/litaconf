Data Type Variables
===================

Variables
---------

Suppose you found an envelope lying on the street and on the front of the envelope was printed the name numberOfDogsTeeth. Suppose further that you opened the envelope and inside was a piece of paper with the number 42 written upon it. What might you conclude from such an encounter? Now suppose you kept walking and found another envelope labeled meaningOfLifeUniverseEverything and, again, upon opening it you found a slip of paper with the number 42 on it. Further down the road, you find two more envelopes, entitled numberOfDotsOnPairOfDice and StatuteOfLibertyArmLength, both of which contain the number 42.

Finally, you find one last envelope labeled sixTimesNine and inside of it you, yet again, find the number 42. At this point, you're probably thinking "somebody has an odd affection for the number 42" but then the times table that is stuck somewhere in the dim recesses of your brain begins yelling at you saying "54! It's 54!". After this goes on for an embarrassingly long time, you realize that 6 * 9 is not 42, but 54. So you cross out the 42 in the last envelope and write 54 instead and put the envelope back where you found it.

This strange little story, believe it or not, has profound implications for writing programs that both humans and computers can understand. For programming languages, the envelope is a metaphor for something called a variable, which can be thought of as a label for a place in memory where a literal value can reside. In other words, a variable can be thought of as a convenient name for a value. In many programming languages, one can change the value at that memory location, much like replacing the contents of an envelope. A variable is our first encounter with a concept known as abstraction, a concept that is fundamental to the whole of computer science.

You will want to start a new python interpreter session if you don't already have one and follow along with these examples. If something doesn’t work, or confuses you, ask.

```
$ python
```

In Python a variable takes the type of the data assigned to it. It will keep that type and you will be warned if you try to mix data in strange ways - like trying to add a string to a number. We can change the type of data that a variable points to by reassigning the variable.

```
>>> answer = "Forty Two"
>>> answer = 42.0 # answer is now a number
>>> print 'answer', answer
answer 42.0
>>> answer = answer + 7.0
>>> print 'new answer',answer
new answer 49.0
>>> meaningofLife
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'meaningofLife' is not defined
>>> meaningofLife = 42
>>> meaningofLife
42
```

Note that `answer` was set to point to the character string "Forty Two" initially. It maintained that value until we made it point to the number "42". Thus, Python variables maintain the type of whatever they point to, but we can change what they point to simply by reassigning the variable. At that point the original data is 'lost' and Python will erase it from memory (unless another variable points at it too) this is known as garbage collection.

Garbage collection can be likened to the mail room clerk who comes round once in a while and removes any packets that are in boxes with no labels. If he can't find an owner or address on the packets he throws them in the garbage. Let's take a look at some examples of data types and see how all of this fits together.

### Exercise

-	In the interpreter, multiply the strings ‘10’ and ‘20’ to get the integer result 200:

Primitive Data Types: Integers and Real Numbers
===============================================

Plain integers in Python are written as strings of digits without commas, periods, or dollar signs. A negative number begins with a single -. Plain integers have range ± 2 billion, or about 9 decimal digits.

We've already seen the basic arithmetic operators one can do in the basics section. Here are the basic arithmetic operations that Python recognizes:

-	Addition (+) is the + character.
-	Subtraction (-) is the - character.
-	Multiplication (×) is the * character.
-	Division (÷) is the / character for standard division, and // for integer-like division.
-	Raising to a power (a^p) is the ** sequence of characters.
-	Modulus (remainder in division) is the % character.
-	Grouping is done with the ( and ) characters.

Here are some examples:

```
>>> 32 - 42
-10
>>> 42 * 19 + 21 / 6
801
>>> 2**10
1024
>>> 241 % 16
1
>>> (18-32)*5/9
-8
>>> var1 = 2     # create an integer and assign it to i1
>>> var2 = 4
>>> var3 = var1 ** var2  # assign the result of 2 to the power 4 to var3
>>> print var3
16
>>> print 2**4  # confirm the result
16

```

Shortcut Operators
------------------

One very common operation that is carried out while programming is incrementing a variable's value. Thus if we have a variable called x with a value of 42 and we want to increase its value to 43 we can do it like this:

```
>>> answer = 42
>>> print answer
>>> answer = answer + 1
>>> print answer
```

Notice the line `answer = answer + 1` This is not sensible in mathematics but in programming it is. What it means is that answer takes on the previous value of answer plus 1. If you have done a lot of math this might take a bit of getting used to, but basically the equal sign in this case could be read as becomes. So that it reads: answer becomes answer + 1.

Now it turns out that this type of operation is so common in practice that Python and many other programming languages provide a shortcut operator to save some typing:

```
>>> answer = 42
>>> answer += 1
>>> print answer
```

Primitive Data Types: Real Numbers
==================================

These include any number that is not a whole number. They are sometimes referred to as floating-point numbers. A floating-point number takes at least eight bytes, making it twice the size of a plain integer. The extra complexity of scientific notation makes them much slower (but barely noticeable for most use) than plain integers. They have about 17 digits of useful precision.

All of the arithmetic operators we saw in Integers, also apply to floating-point numbers. Here are a couple of examples:

```
>>> 41.9
41.9
>>> 42.
42.0
>>> 0.3
0.3
>>> 3.0e-4
0.0003
```

Python can also handle complex numbers (including matrices) natively. We will ignore that for our purposes but the documentation is available at the [Python Website](http://python.org)

### Exercises

Loops
=====

for Loops
---------

A program may have a goal that is best described using the words “for all”, where we have to do some calculation for all values in a set of values. What we are going to do is get Python to do the repetition, substituting a variable which increases or decreases in value each time it repeats. It looks like this in pseudo-code:

```
:::python
for i is a value in some set:
    compute mapped result based on i
```

The Python "for" loop iterates over a sequence. A Sequence in Python, is either a string, a list or a tuple. So we can write for loops that act on any of those. Let's try printing the letters of a word one by one using a for loop with a string:

```
>>> for characters in 'word': print c
...
w
o
r
d
>>> for item in ['one', 2, 'three']: print item
...
one
2
three
>>> for word in ('one','word', 'after', 'another'): print word
...
one
word
after
another
```

Notice how the letters were printed, one per line in the first string example. Notice too that where the body of the loop consists of a single line we can add it on the same line after the colon(:). The colon is what tells Python that there's a block of code coming up next.

There is one caveat when using foreach style loops like these. The loop gives you a copy of what was in the collection, you can't modify the contents of the collection directly. So if you need to modify the collection you have to use an awkward kludge involving the index of the collection, like this:

```
:::python
myOtherList = [1,2,3,4,5]
for index in range(len(myOtherList)):
    myOtherList[index] += 1
print myOtherList
```

Note that in this example I have not used the interactive Python prompt (>>>), so you need to type this into a file that you will run. If you do try typing it at the >>> prompt you will need to add extra blank lines to tell Python when you finish a block, for example after the myList = line. It's actually quite a good way of learning where blocks start and stop; to type the code in and see if you correctly guess where an extra line will be needed. It should be where the indentation changes.

The other gotcha with for loops is that you can't delete items from the collection that you are iterating over, otherwise the loop will get confused. It's a bit like the old cartoon character cutting off the branch of a tree while sitting on it. The best way to deal with this situation is to use a different kind of loop, which we are going to discuss next. However to understand how to remove elements safely we need to wait until we cover yet another topic, that of branching, so we will explain this subject when we get there.

while Loops
-----------

One way to handle the “search for the first” case is with the while statement. This can also be used to do a “for-all” mapping, reduction or filtering. The while statement looks like this:

```
:::python
while expression :
    suite
```

The suite is an indented block of statements. Any statement is allowed in the block, including indented **while** statements.

As long as the expression is true, the suite is executed. This allows us to construct a suite that steps through all of the necessary tasks to reach a terminating condition. It is important to note that the suite of statements must include a change to at least one of the variables in the **while** expression. Should your program execute the suite of statements without changing any of the variables in the **while** expression, nothing will change, and the loop will not terminate.

```
>>> x = 10 #initialize
>>> while x > 0:
...     print x
...     x -= 1 # this is a short cut for x = x - 1
```

..

Collection Types: Dictionaries
==============================

In the same way that a dictionary associates a meaning with a word a dictionary type contains a value associated with a key. The value can be retrieved by 'indexing' the dictionary with the key. The key doesn't need to be a character string (although it often is) but can be any immutable type including numbers and tuples. Similarly the values associated with the keys can have any kind of data type. Dictionaries are usually implemented internally using an advanced programming technique known as a hash table. For that reason a dictionary may sometimes be referred to as a hash.

```
:::bash
>>> dictionary = {} #initialize the dictionary
>>> dictionary ['boolean'] = "A value which is either true or false"
>>> dictionary ['integer'] = "A whole number"
>>> dictionary ['boolean'] = "A whole number"
A value which is either true or false
>>> addressBook = {
... 'UIC' : ['UIC Libraries', '801 S. Morgan St',' Chicago', 'IL', '60607'],
... 'UMD' : ['UMD Libraries', 'B0131 McKeldin', 'College Park', 'MD', '20742']
... }
>>>
```

The key and value are separated by a colon and the pairs are separated by commas. The second dictionary with address book is made out of a dictionary which is keyed by name and stores lists as the values. Rather than work out the numerical index of the entry we want we can just use the name to retrieve all the information, like this:

```
>>> addressBook['UIC']
['UIC Libraries', '801 S. Morgan St', ' Chicago', 'IL', '60607']
>>> print addressBook['UMD'][2]
College Park
```

Due to their internal structure dictionaries do not support very many of the collection operators that we've seen so far. None of the concatenation, repetition or appending operations work. To assist us in accessing the dictionary keys there is an operation that we can use, keys(), which returns a list of all the keys in a dictionary. For example to get a list of all the names in our address book we could do:

```
:::bash
>>> print addressBook.keys()
['UMD', 'UIC']
```

Note however that dictionaries do not store their keys in the order in which they are inserted so you may find the keys appear in a strange order, indeed the order may even change over time. Don't worry about that, you can still use the keys to access your data and the right value will still come out OK.

### Exercises

-	loop over a list of strings, split into key: value pairs and add to dictionary
-	Create a dictionary to hold information about pets. Each key is an animal's name, and each value is the kind of animal.

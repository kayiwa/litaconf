Boolean Values: True and False
==============================

Like the heading says, this type has only 2 values - either true or false. Python supports Boolean values directly, using the values True and False. Python has a number of representations for truth and falsity. While we’re mostly interested in the basic Python literal of False and True, there are several alternatives.

-	**False**: Also 0, the complex number 0+0j, the special value **None**, zero-length strings "", zero-length lists [], zero-length tuples (), and empty mappings {} are all treated as False. We’ll return to these list, tuple and map data structures in later chapters. For now, we only need to know that a structure that is empty of meaningful content is effectively False.
-	**True**: Anything else that is not equivalent to False. This means that any non-zero number, or any string with a length of one or more characters are equivalent to **True**.

In Python, the data values of True and False, plus the operators not, and and or define a data type.

```
>>> bool( 0 )
False
>>> bool( 1 )
True
>>> bool( [] )
False
>>> bool( ['this'] )
True
>>> bool( 0.0 )
False
>>> bool( 1.0 )
True
>>> bool( '' )
False
>>> bool( 'this' )
True
>>> bool( None )
False
```

..

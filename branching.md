Branching
=========

The `if` statement
------------------

The most intuitively obvious conditional statement is the if, then, else construct. It follows the logic of English in that if some boolean condition (see below for more about this aspect of things) is true then a block of statements is executed, otherwise (or else) a different block is executed.

An example of this below:

```
:::python
import sys  # only to let us exit
print "Starting here"
j = 5
if j > 10:
    print "This is never printed"
else:
    sys.exit()
```

Boolean Expressions
-------------------

You might remember that in the [Boolean type](litaconf/src/main/datatypesbool.md) of data section we said it had only two values: True or False. We very rarely create a Boolean variable but we often create temporary Boolean values using expressions. An expression is a combination of variables and values combined by operators to produce a resultant value. In the following example:

```
:::python
if x < 42:
    print x
```

`x < 42` is the expression and the result will be True if x is less than 42 and False if x is greater than or equal to 42.

Expressions can be arbitrarily complex provided they evaluate to a single final value. In the case of a branch that value must be either True or False. In many programming languages False is the same as 0 or a non-existent value (often called NULL, Nil or None). Thus an empty list or string evaluates to false in a Boolean context. Python works this way and this means we can use a while loop to process a list until the list is empty, using something like:

```
:::python
while anotherList:
    # do the following
```

Boolean operators can be used to reduce the number of `if` statements.

Chaining if statements
----------------------

One can go on to chain these if/then/else statements together by nesting them one inside the other. Here is an example:

```
:::python
# Assume created previously...
price = int(raw_input("iPhone price? "))
if price == 300:
    print "I'll take it!"
else:
    if price > 600:
        print "No way Tim Cook!"
    else:
        if price > 400:
            print "How about throwing in a free data rates?"
        else:
            print "price is an unexpected value!"
```

**Note 1:** we used == (that's a double = sign) to test for equality in the first if statement, whereas we use = to assign values to variables. Using = when you mean to use == is one of the more common mistakes in programming Python, fortunately Python warns you that it's a syntax error, but you might need to look closely to spot the problem.

**Note 2:** A subtle point to notice is that we perform the greater-than tests from the highest value down to the lowest. If we did it the other way round the first test, which would be price > 400 would always be true and we would never progress to the > 600 test. Similarly if using a sequence of less-than tests you must start at the lowest value and work up. This is another very easy trap to fall into.

Case Statements
---------------

Python generally is easy to read until you have a series of `if/else` statements that run across your page when you chain them together like we did above. Python solves this using the `if/elseif/else` format.

```
:::python
menu = """
Pick a shape(1-3):
    1) Square
    2) Rectangle
    3) Triangle
"""

shape = int(raw_input(menu))
if shape == 1:
    length = float(raw_input("Length: "))
    print "Area of square = ", length ** 2
elif shape == 2:
    length = float(raw_input("Length: "))
    width = float(raw_input("Width: "))
    print "Area of rectangle = ", length * width
elif shape == 3:
    length = float(raw_input("Length: "))
    width = float(raw_input("Width: "))
    print "Area of triangle = ", length * width/2
else:
    print "Not a valid shape, try again"
```

The example above is much more readable with the `elif` in it.

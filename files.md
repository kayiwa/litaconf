Handling Files
==============

Open the 'journals.csv' file in your text editor. Your text editor holds the entire file in memory while you work on it and then writes it all back out when you close it. When you open a file in Python you will be opening it 'usually' as read only or 'write only' or append to the existing file.

Let's poke around this file to read it's contents

```
>>> reader = open( 'journals.csv', 'r') # r is for "read" mode
>>> reader
<open file 'journals.csv', mode 'r' at 0x1076f4ed0> # loaded into memory
>>> content = reader.read()
>>> len(content)
474
>>> reader.close()
```

While in the directory let's try to create a new file from inside the interpreter.

```
>>> writer = open('interactivetextfile.txt', 'w') # this is in "write mode"
>>> writer
<open file 'interactivetextfile.txt', mode 'w' at 0x106555ed0>
>>> content = writer.write('write to the new file') # writes to file above
>>> content.close()
```

If you close the python interactive shell you will now see a new file falled 'interactivetextfile.txt' with the content you typed.

Working with files one can use the following simple steps can be written to imitate the unix `cat` command or the Microsoft Windows `type` command both of which read and display the contents of a file. Create a new python program file called "pycat.py" and add the following content

```
:::python
#!/usr/bin/env python
# First open the file to read(r)
input = open("journals.csv","r")
# iterate over the file and print each item
for line in input:
    print line
# Close up
input.close()
```

Save the file and make sure you are in the same directory that has the 'journals.csv' file and run the program.

```
$ python pycat.py
identifier,title,language

47918-4,C't,ger

54251-9,C't,ger

246797-5,UNIX-Magazin,ger

1013182-6,IX,ger

1307745-4,C't-ROM,ger
...
$ cat journals.csv
identifier,title,language
47918-4,C't,ger
54251-9,C't,ger
246797-5,UNIX-Magazin,ger
1013182-6,IX,ger
1307745-4,C't-ROM,ger
...
```

The ellipses are a truncation of both your new python version of the cat utility. There's one slight different between these results that can be fixed with the rstrip() string method.

For our second example let's create a copy of our 'journal.csv' file using python. Create a new file name pycp.py with the following content.

```
:::python
#!/usr/bin/env python
# Create the equivalent of unix cp command

# First open the files to read(r) and write(w)
input = open("journals.csv","r")
output = open("journals.csv.bk","w")

# read file, copying each line to new file
for line in input:
    output.write(line)

print "1 file copied..." # this will echo progress

# Now close the files which is a good practice
input.close()
output.close()
```

Run `python pycp.py` from your shell and notice the addition of a new backup of the original 'journals.csv' file.

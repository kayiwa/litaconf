#!/usr/bin/env python
# Licensed under the ISC license
# http://opensource.org/licenses/ISC

# Python with Library Data Class

"""A Python program for exercise 0 to check that Python is working.
Try running this program from the command line like this:
  python hello.py
  python hello.py Melvil
That should print:
  Hello World -or- Hello Melvil
Try changing the 'Hello' to 'Howdy' and run again.
Once you have that working, you're ready for class -- you can edit
and run Python code; now you just need to learn Python!
"""

import sys

# Define a main() function that prints a little greeting.
def main():
# Get the name from the command line, using 'World' as a fallback.
    if len(sys.argv) >= 2:
        name = sys.argv[1]
    else:
        name = 'World'
    print 'Hello', name

# Calls the main() function.
if __name__ == '__main__':
    main()

README
======

This repository is about a class taught at the LITA Forum 2014. The goal was to guide users new to Python on how to hack library data. If successful this document will be improved on by class participants and others

### What is this repository for?

-	Version Control
-	Python Programming
-	[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Activites for Day One

-	[Simple Git](litaconf/src/master/git.md)
-	[Basics](litaconf/src/master/basics.md)
-	[Variables](litaconf/src/master/datatypesvariable.md)
-	[Data Type: Strings](litaconf/src/master/datatypesstrings.md)
-	[Data Type: Integers and Real Numbers](litaconf/src/master/datatypesintegers.md)
-	[Collections: Lists and Tuples](litaconf/src/master/collists.md)
-	[Collections: Dictionaries](litaconf/src/master/coldict.md)
-	[Loops](litaconf/src/master/loops.md)

### Activities for Day Two

-	[Branching](litaconf/src/master/branching.md)
-	[Functions](litaconf/src/master/functions.md)
-	Modules
-	[Handling Files](litaconf/src/master/files.md)
-	[Handling Text](litaconf/src/master/text.md)
-	[Error Handling](litaconf/src/master/error.md)

### Contribution guidelines

-	Fork the Repository. Submit Pull Request.
-	Code review

### Who do I talk to?

-	[Francis Kayiwa](https://twitter.com/kayiwa)
